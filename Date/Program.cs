﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Date
{
    public class Date
    {
        private int day;
        private int month;
        private int year;

        public Date()
        {
            day = 5;
            month = 12;
            year = 2002;
        }

        public Date(int day, int month, int year)
        {
            this.day = day;
            this.month = month;
            this.year = year;
        }

        public int getDay()
        {
            return day;
        }

        public int getMonth()
        {
            return month;
        }

        public int getYear()
        {
            return year;
        }

        public void setDay(int day)
        {
            this.day = day;
        }

        public void setMonth(int month)
        {
            this.month = month;
        }

        public void setYear(int year)
        {
            this.year = year;
        }

        public void setDate(int day, int month, int year)
        {
            this.day = day;
            this.month = month;
            this.year = year;
        }

        public override string ToString()
        {
            return "Date is [day." + day + " month." + month + " year" + year + "]";
        }


    }
    public class Program
    {
        static void Main(string[] args)
        {
            Date päivämäärä = new Date();
            Console.WriteLine(päivämäärä.ToString());
        }
    }
}
