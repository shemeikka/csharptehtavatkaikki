﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoiceitem
{
    public class Invoiceitem
    {
        private string id;
        private string desc;
        private int qty;
        private double unitPrice;

    public Invoiceitem()
        {
            id = "aaa";
            desc = "bbb";
            qty = 5;
            unitPrice = 6;
        }

        public Invoiceitem(string id, string desc, int qty, double unitPrice)
        {
            this.id = id;
            this.desc = desc;
            this.qty = qty;
            this.unitPrice=unitPrice;
        }

        public string getID()
        {
            return id;
        }

        public string getDesc()
        {
            return desc;
        }

        public int getQty()
        {
            return qty;
        }

        public void setQty(int qty)
        {
            this.qty = qty;
        }

        public double getUnitPrice()
        {
            return unitPrice;
        }

        public void setUnitPrice(double unitPrice)
        {
            this.unitPrice = unitPrice;
        }

        public double getTotal()
        {
            return unitPrice * qty; ;
        }

        public override string ToString()
        {
            return "Invoiceitem[id=" + id + ",desc=" + desc + ",qty=" + qty + ",unitPrice=" + unitPrice +"]";
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            Invoiceitem laa = new Invoiceitem();
            Console.WriteLine(laa);
        }
    }
}
