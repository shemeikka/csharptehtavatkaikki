﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace circle
{
    public class Circle
    {
        private double radius = 1.0;
        private String color = "red";

        public Circle()
        {
            radius = 1.0;
            color = "red";
        }

        public Circle(double radius)
        {
            this.radius = radius;
        }

        public double getRadius()
        {
            return radius;
        }

        public double getArea()
        {
            return radius * radius * Math.PI;
        }

        public Circle(double radius, String color)
        {
            this.radius = radius;
            this.color = color;
        }

        public String getColor()
        {
            return color;
        }

        public void setRadius(double newRadius)
        {
            radius = newRadius;
        }

        public void setColor(String newColor)
        {
            color = newColor;
        }
    }

    public class TestCircle
    {
        static void Main(string[] args)
        {
            Circle c1 = new Circle();
            Console.WriteLine("The Circle has radius of " + c1.getRadius() + " and area of " + c1.getArea());

            Circle c2 = new Circle();
            Console.WriteLine("The Circle has radius of " + c2.getRadius() + " and area of " + c2.getArea());
        }
    }
}
