﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account
{
    public class Account
    {
        string id;
        string name;
        int balance;

        public Account()
        {
            id = "bla";
            name = "plo";
            balance = 20;
        }

        public Account(string id, string name, int balance)
        {
            this.id = id;
            this.name = name;
            this.balance = balance;

        }

        public string getID()
        {
            return id;
        }

        public string getName()
        {
            return name;
        }

        public int getBalance()
        {
            return balance;
        }
        public int credit(int amount)
        {
            return balance= balance + amount;
        }

        public int debit(int amount)
        {
            if (amount <= balance)
            {
                balance = balance - amount;
                return balance;
            }
            else
            {
                Console.WriteLine("Amount exceeded balance");
                return balance;
            }
        }

        public int transferTo(Account another, int amount)
        {
            if(amount<=balance)
            {
                balance = balance - amount;
                another.balance = another.balance + amount;
                return balance;
            }
            else
            {
                Console.WriteLine("Amount exceeded balance");
                return balance;
            }
        }

        public override string ToString()
        {
            return "Account[id=" + id + ",name=" + name + ",balance=" + balance + "]";
        }
    }
    public class Program
    {
        static void Main(string[] args)
        {
            Account glob = new Account();
            Console.WriteLine(glob);

            glob.credit(200);
            Console.WriteLine(glob.getBalance());

            Account glob2 = new Account();

            glob2.debit(200);
            Console.WriteLine(glob2.getBalance());
        }
    }
}
