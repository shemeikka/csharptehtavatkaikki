﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace circletoinen
{
    public class Circle2
    {
        private double radius = 5;

        public Circle2()
        {
            radius = 5;
        }

        public Circle2(double radius)
        {
            this.radius = radius;
        }

        public double getRadius()
        {
            return radius;
        }

        public void setRadius(double newRadius)
        {
            radius = newRadius;
        }

        public double getArea()
        {
            return radius * radius * Math.PI;
        }

        public double getCircumference()
        {
            return 2 * Math.PI * radius;
        }

    }

    public class TestCircle
    {
        static void Main(string[] args)
        {
            Circle2 ympyrä = new Circle2();
            Console.WriteLine("The Circle has radius of " + ympyrä.getRadius() + " and area of " + ympyrä.getArea() + " and circumference of " + ympyrä.getCircumference());

        }
    }
}