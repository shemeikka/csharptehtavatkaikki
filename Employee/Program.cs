﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee
{
    public class Employee
    {
        private int id;
        private String firstName;
        private String lastName;
        private int salary;

        public Employee()
        {
            id = 123;
            firstName = "Anna";
            lastName = "Anna";
            salary = 100000;
        }

        public Employee(int id, String firstName, String lastName, int salary)
        {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.salary = salary;
        }

        public int getID()
        {
            return id;
        }

        public String getFirstName()
        {
            return firstName;
        }

        public String getLastName()
        {
            return lastName;
        }
        
        public String getName()
        {
            return firstName + lastName;
        }

        public int getSalary()
        {
            return salary;
        }

        public void setSalary(int salary)
        {
            this.salary = salary;
        }
        public int getAnnualSalary()
        {
            return salary * 12;
        }

        public override String ToString()
        {
            return "Employee [id=" + id + " name=" + firstName + lastName + " salary=" + salary + "]";
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Employee yksi = new Employee();
            Console.WriteLine(yksi.ToString());
        }
    }
}
