﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neliö
{
    public class Rectangle
    {
        private float length;
        private float width;

        public Rectangle()
        {
            length = 1.0f;
            width = 1.0f;
        }

        public Rectangle(float length, float width)
        {
            this.length = length;
            this.width = width;
        }

        public float getLength()
        {
            return length;
        }

        public void setLength(float length)
        {
            this.length = length;
        }

        public float getWidth()
        {
            return width;
        }

        public void setWidth(float width)
        {
            this.width = width;
        }

        public double getArea()
        {
            return length * width;
        }

        public double getPerimeter()
        {
            return length + width + length + width;
        }

        public string toString()
        {
            return "Rectangle [length=" + length + " width =" + width + " area =" + getArea() + "]";
        }
    }


class Program
    {
        static void Main(string[] args)
        {
            Rectangle neliö = new Rectangle(30, 50);

            Console.WriteLine(neliö.toString());

            Rectangle neliö2 = new Rectangle();

            neliö2.setLength(25);
            neliö2.setWidth(5);

            Console.WriteLine(neliö2.toString());
        }
    }
}
